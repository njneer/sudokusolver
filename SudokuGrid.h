#ifndef SUDOKUGRID_H
#define SUDOKUGRID_H

#include<vector>
#include<SudokuUtils.h>

class SudokuGrid {
	public:
		SudokuGrid();
		~SudokuGrid();

		bool solve();

		void setCell(const int row, const int col, SudokuUtils::cell cellData);

		void setCellValue(const int row, const int col, int value);

		int getCellValue(const int row, const int col);

		void getMaxRowsCols(int &rows, int &cols) const;

		virtual bool findUnfilledCell(int &row, int &col);

		void print(const char *fileName = 0);

		void removeInvalidOptions();

		virtual bool constraintsMet(const int row, const int col, const int value);

		static const int NUM_ROWS;
		static const int NUM_COLS;

	protected:

		//CSP Constraints

		bool valueNotRepeatedInRow(const int row, const int col, const int value);

		bool valueNotRepeatedInCol(const int row, const int col, const int value);

		bool valueNotRepeatedInSubGrid(const int gridStartRow, const int gridStartCol, const int value);

		bool validRow(const int row);
		bool validCol(const int col);

		SudokuUtils::cell **_gridCells;

	private:

		int _backTracks;
		


};

#endif