#include <HyperSudokuGrid.h>
#include <iostream>

HyperSudokuGrid::HyperSudokuGrid():
	SudokuGrid()
{
}

HyperSudokuGrid::~HyperSudokuGrid(){


}
#ifdef HYPERSUDOKUHEURISTIC
//Implement Minimum Remaining Value (MRV) heuristic
bool HyperSudokuGrid::findUnfilledCell(int &row, int &col){

	int min = SudokuGrid::NUM_ROWS + 1;
	int curRow = -1;
	int curCol = -1;

	bool foundCell = false;

	for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
		for(int j = 0; j < SudokuGrid::NUM_COLS; ++j){
			if(_gridCells[i][j].currValue == 0 && !_gridCells[i][j].locked && _gridCells[i][j].availOptions.size() < min){
				curRow = i;
				curCol = j;
				min = _gridCells[i][j].availOptions.size();
			}
		}
	}
	if(curRow >= 0 && curCol >= 0){
		row = curRow;
		col = curCol;
		foundCell = true;
	}
	return foundCell;
}
#endif

bool HyperSudokuGrid::constraintsMet(const int row, const int col, const int value){

	bool constraintMet = false;

	if(validRow(row) && validCol(col)){
		int gridStartRow = row - row%3;
		int gridStartCol = col - col%3;

		int hyperRow = -1;
		int hyperCol = -1;

		if(row >= 1 && row <= 3){
			hyperRow = 1;
		} else if(row >= 5 && row <= 7){
			hyperRow = 5;
		}

		if(col >= 1 && col <= 3){
			hyperCol = 1;
		} else if(col >= 5 && col <= 7){
			hyperCol = 5;
		}

		if(hyperRow > 0 && hyperCol > 0){
			constraintMet = valueNotRepeatedInRow(row, col, value) && valueNotRepeatedInCol(row, col, value) 
						&& valueNotRepeatedInSubGrid(gridStartRow, gridStartCol, value) &&
						valueNotRepeatedInSubGrid(hyperRow, hyperCol, value);

		} else {
			constraintMet = valueNotRepeatedInRow(row, col, value) && valueNotRepeatedInCol(row, col, value) 
						&& valueNotRepeatedInSubGrid(gridStartRow, gridStartCol, value);
		}
	}
	return constraintMet;
}