#include <iostream>
#include <fstream>
#include <string>
#include <SudokuGrid.h>
#include <HyperSudokuGrid.h>

using namespace std;

bool initGrid(string fileName, SudokuGrid& grid);
bool readFile(string fileName, int fileValues[][9]);

int main(int argc, const char *argv[])
{
  string inputFile;
  string outputFile;

  #ifdef SUDOKU
  	SudokuGrid gameGrid;
  #else 
  	HyperSudokuGrid gameGrid;
  #endif

  //User did not enter text files
  if(argc !=3){
  	cout << "ERROR - Must enter input & output files!\n";
  	exit(-1);
  }

  inputFile = argv[1];
  outputFile = argv[2];

  bool initDone = false;

  initDone = initGrid(inputFile, gameGrid);

  if(initDone){
  	if(gameGrid.solve()){
  		cout << "Solved: \n";
  		gameGrid.print(outputFile.c_str());
  		gameGrid.print();
  	} else {
  		cout << "Unsolvable\n";
  	}
  }

}

bool initGrid(string fileName, SudokuGrid& grid){
	bool initComplete = false;

	int rows, cols;

	grid.getMaxRowsCols(rows, cols);

	int gameValues[9][9];


	initComplete = readFile(fileName, gameValues);

	//Initialize SudokuGrid
	if(initComplete){
		for(int i = 0; i < rows; ++i){
			for(int j = 0; j < cols; ++j){
				bool isLocked = false;
				int currValue = gameValues[i][j];
				if(currValue != 0){
					isLocked = true;
				}
				SudokuUtils::cell currCell;
				SudokuUtils::initCell(currCell, currValue, isLocked);
				grid.setCell(i, j, currCell);
			}
		}
		grid.removeInvalidOptions();
	}
	return initComplete;
}

bool readFile(string fileName, int fileValues[][9]){

	const int MAX_LINE_LEN = 20;
	const char* DELIMETER = " ";

	int currentRow = 0;

	ifstream fileIn;
	fileIn.open(fileName.c_str());

	if(!fileIn.good()){
		return false;
	}

	while(!fileIn.eof()){
		char buffer [MAX_LINE_LEN];

		fileIn.getline(buffer, MAX_LINE_LEN);

		int n = 0;

		const char* values[MAX_LINE_LEN] = {};

		values[0] = strtok(buffer, DELIMETER);

		//Non-Blank
		if(values[0]){
			for(n = 1; n < MAX_LINE_LEN; ++n){
				values[n] = strtok(0, DELIMETER);
				if(!values[n]){
					break;
				}
			}
		}

		//Print out all the values found
		for(int i = 0; i < n; ++i){
			if(!strcmp(values[i], "-")){
				fileValues[currentRow][i] = 0;
			} else {
				fileValues[currentRow][i] = atoi(values[i]);
			}
		}

		currentRow++;
	}
	return true;
}