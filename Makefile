CC = g++
INCLUDE = .
CFLAGS += -g -I$(INCLUDE) -D SUDOKU
CFLAGS1 += -g -I$(INCLUDE) -D HYPERSUDOKU
CFLAGS2 += -g -I$(INCLUDE) -D HYPERSUDOKUHEURISTIC
LDFLAGS += 

SRCS = \
	SudokuUtils.cpp \
	SudokuGrid.cpp \
	SudokuSolver.cpp

SRCS1 = \
	SudokuUtils.cpp \
	SudokuGrid.cpp \
	HyperSudokuGrid.cpp \
	SudokuSolver.cpp

PROGRAM = SudokuSolver
PROGRAM1 = HyperSudokuSolver
PROGRAM2 = HyperSudokuSolverHeuristic

OBJ_DIR = obj/

#Commands
MKDIR = MKDIR
RM = rm -f

OBJECTS = $(SRCS:%.cpp=$(OBJ_DIR)%.o)
HYPEROBJECTS = $(SRCS1:%.cpp=$(OBJ_DIR)%.hyper)
HYPERHEUOBJECTS = $(SRCS1:%.cpp=$(OBJ_DIR)%.hyperheu)

default: all

all:	$(PROGRAM)

$(PROGRAM): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $(OBJECTS) $(LDFLAGS)

$(PROGRAM1): $(HYPEROBJECTS)
	$(CC) $(CFLAGS1) -o $@ $(HYPEROBJECTS) $(LDFLAGS)

$(PROGRAM2): $(HYPERHEUOBJECTS)
	$(CC) $(CFLAGS2) -o $@ $(HYPERHEUOBJECTS) $(LDFLAGS)

clean:
	$(RM) -r $(OBJECTS) $(HYPEROBJECTS)
	$(RM) $(PROGRAM) $(PROGRAM1) $(PROGRAM2)

$(OBJ_DIR)%.o:%.cpp
		@if [ -d $(OBJ_DIR) ]; then set +x; \
		else (set -x; $(MKDIR) -p $(OBJ_DIR)); fi
		-$(RM) $@
		@echo ;
		@echo "########### Building ... $@ ########### ";
		@echo ;
		$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)%.hyper:%.cpp
		@if [ -d $(OBJ_DIR) ]; then set +x; \
		else (set -x; $(MKDIR) -p $(OBJ_DIR)); fi
		-$(RM) $@
		@echo ;
		@echo "########### Building ... $@ ########### ";
		@echo ;
		$(CC) $(CFLAGS1) -c $< -o $@

$(OBJ_DIR)%.hyperheu:%.cpp
		@if [ -d $(OBJ_DIR) ]; then set +x; \
		else (set -x; $(MKDIR) -p $(OBJ_DIR)); fi
		-$(RM) $@
		@echo ;
		@echo "########### Building ... $@ ########### ";
		@echo ;
		$(CC) $(CFLAGS2) -c $< -o $@