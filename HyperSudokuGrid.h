#ifndef HYPERSUDOKUGRID_H
#define HYPERSUDOKUGRID_H

#include <SudokuGrid.h>

class HyperSudokuGrid: public SudokuGrid{

	public:
		HyperSudokuGrid();
		~HyperSudokuGrid();

		virtual bool constraintsMet(const int row, const int col, const int value);

		#ifdef HYPERSUDOKUHEURISTIC
		virtual bool findUnfilledCell(int &row, int &col);
		#endif

	protected:

	private:


};


#endif