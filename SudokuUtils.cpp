#include <SudokuUtils.h>
#include <iostream>

namespace SudokuUtils{

void initCell(SudokuUtils::cell &theCell, int cellValue, bool isLocked){
	theCell.currValue = cellValue;
	theCell.locked = isLocked;

	for(int i = 1; i < 10; ++i){
		if(i != cellValue){
			option currOption;
			currOption = i;
			theCell.availOptions.push_back(currOption);
		}
	}
};

void printCell(const int row, const int col, SudokuUtils::cell &theCell){
	printf("(%d,%d): %d\n", row, col, theCell.currValue);
}

};