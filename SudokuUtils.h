#ifndef SUDOKU_UTILS_H
#define SUDOKU_UTILS_H

#include<vector>

namespace SudokuUtils{

typedef int option;

typedef struct {
	int currValue;
	std::vector<option> availOptions;
	bool locked; //If cell is initialized with a value, it cannot be changed.
} cell;

void initCell(SudokuUtils::cell &theCell, int cellValue, bool isLocked);

void printCell(const int row, const int col, SudokuUtils::cell &theCell);


};
#endif