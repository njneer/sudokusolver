#include <SudokuGrid.h>
#include <iostream>
#include <fstream>

const int SudokuGrid::NUM_ROWS = 9;
const int SudokuGrid::NUM_COLS = 9;

SudokuGrid::SudokuGrid()
{
	_gridCells = new SudokuUtils::cell*[SudokuGrid::NUM_ROWS];
	for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
		_gridCells[i] = new SudokuUtils::cell[SudokuGrid::NUM_COLS];
	}
	_backTracks = 0;
}

SudokuGrid::~SudokuGrid(){


}

void SudokuGrid::print(const char *fileName){
	int value;

	if(fileName != NULL){
		std::ofstream fileOut(fileName);
		if(fileOut.is_open()){
			for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
				for(int j = 0; j < SudokuGrid::NUM_COLS; ++j){
					value = getCellValue(i, j);
					fileOut << value << " ";
				}
				fileOut << std::endl;
			}
			fileOut.close(); 
		}
	} else {
		std::cout << "Backtracks: " << _backTracks << std::endl;
		for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
			for(int j = 0; j < SudokuGrid::NUM_COLS; ++j){
				value = getCellValue(i, j);
				std::cout << value << " ";
			}
			std::cout << std::endl;
		}
	}
}

bool SudokuGrid::solve(){
	int row, col;

	//Find available cell, no empty cells, puzzle done. Recursive terminal condition
	if(!findUnfilledCell(row, col)){
		return true;
	}

	//Available cell, try to place value & check constraints
	std::vector<SudokuUtils::option>::iterator it;
	for(it = _gridCells[row][col].availOptions.begin(); 
		it != _gridCells[row][col].availOptions.end(); ++it){
		if(constraintsMet(row, col, *it)){

			setCellValue(row, col, *it);

			//This cell set, go on to next cell and try
			if(solve()){
				return true;
			} else {
				//The rest of the puzzle couldn't be solved
				// with this cells value, try the next available value.
				setCellValue(row, col, 0); //Reset cell
			}

		}
	}
	//Tried all possible values for this cell, didn't work, backtrack
	_backTracks++;

	return false;
}

bool SudokuGrid::findUnfilledCell(int &row, int &col){
	for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
		for(int j = 0; j < SudokuGrid::NUM_COLS; ++j){
			if(_gridCells[i][j].currValue == 0 && !_gridCells[i][j].locked){
				row = i;
				col = j;
				return true;
			}
		}
	}
	return false;
}

void SudokuGrid::getMaxRowsCols(int &rows, int &cols) const{
	rows = SudokuGrid::NUM_ROWS;
	cols = SudokuGrid::NUM_COLS;
}

void SudokuGrid::setCell(const int row, const int col, SudokuUtils::cell cellData){
	_gridCells[row][col] = cellData;
}

void SudokuGrid::setCellValue(const int row, const int col, int value){
	_gridCells[row][col].currValue = value;
}

bool SudokuGrid::validRow(const int row){
	bool valid = false;
	if(row >= 0 && row <= SudokuGrid::NUM_ROWS){
		valid = true;
	}
	return valid;
}

bool SudokuGrid::validCol(const int col){
	bool valid = false;
	if(col >= 0 && col <= SudokuGrid::NUM_COLS){
		valid = true;
	}
	return valid;
}

//Should be used to check a value would be valid in a cell BEFORE it is actually placed.
bool SudokuGrid::valueNotRepeatedInRow(const int row, const int col, const int value){
	bool constraintMet = true;

	if(validRow(row) && validCol(col)){
		for(int i = 0; i < SudokuGrid::NUM_COLS; ++i){
			int checkValue = _gridCells[row][i].currValue;
			if(checkValue == value){
				constraintMet = false;
				break;
			}
		}
	}

	return constraintMet;
}

bool SudokuGrid::valueNotRepeatedInCol(const int row, const int col, const int value){
	bool constraintMet = true;

	if(validRow(row) && validCol(col)){
		for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
			int checkValue = _gridCells[i][col].currValue;
			if(checkValue == value){
				constraintMet = false;
				break;
			}
		}
	}

	return constraintMet;
}

bool SudokuGrid::valueNotRepeatedInSubGrid(const int gridStartRow, const int gridStartCol, const int value){
	bool constraintMet = true;

	const int RowColSize = 3;

	if(validRow(gridStartRow) && validCol(gridStartCol)){
		
		for(int i = 0; i < RowColSize; ++i){
			for(int j = 0; j < RowColSize; ++j){
				int checkValue = _gridCells[i + gridStartRow][j + gridStartCol].currValue;
				if(checkValue == value){
					constraintMet = false;
					break;
				}
			}
		}
	}

	return constraintMet;
}

bool SudokuGrid::constraintsMet(const int row, const int col, const int value){
	bool constraintMet = false;

	if(validRow(row) && validCol(col)){
		int gridStartRow = row - row%3;
		int gridStartCol = col - col%3;

		constraintMet = valueNotRepeatedInRow(row, col, value) && valueNotRepeatedInCol(row, col, value) 
						&& valueNotRepeatedInSubGrid(gridStartRow, gridStartCol, value);
	}

	return constraintMet;
}

int SudokuGrid::getCellValue(const int row, const int col){
	return _gridCells[row][col].currValue;
}


void SudokuGrid::removeInvalidOptions(){
	for(int i = 0; i < SudokuGrid::NUM_ROWS; ++i){
		for(int j = 0; j < SudokuGrid::NUM_COLS; ++j){
			if(!_gridCells[i][j].locked){
				std::vector<SudokuUtils::option> validOptions;
				std::vector<SudokuUtils::option>::iterator it;
				for(it = _gridCells[i][j].availOptions.begin(); 
					it != _gridCells[i][j].availOptions.end();
					++it){
					if(constraintsMet(i, j, *it)){
						validOptions.push_back(*it);
					}
				}
				_gridCells[i][j].availOptions = validOptions;
			}
		}
	}
}