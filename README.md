SudokuSolver
------------

Solves Sudoku's utilizing CSP (Constraint Satisfaction Problem) concepts.

This project is part of CSCI 561 - The University of Southern California.

------------
Building/Running:

SudokuSolver
-----
    Build: make SudokuSolver
    Run: ./SudokuSolver input1.txt output1.txt

HyperSudokuSolver
-----
    Build: make HyperSudokuSolver
    Run: ./HyperSudokuSolver input2.txt output2.txt
    
HyperSudokuSolverHeuristic
-----
    Build: make HyperSudokuSolverHeuristic
    Run: ./HyperSudokuSolverHeuristic input2.txt output2.txt   